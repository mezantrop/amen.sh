#!/bin/sh

# "THE BEER-WARE LICENSE" (Revision 42):
# <zmey20000@yahoo.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.    Mikhail Zakharov

# amen demonstration script

# -- HISTORY ------------------------------------------------------------------
# 2019.03.24	v0.1	zmey20000@yahoo.com	Initial release


. ./amen.sh

BANNER_1="Welcome to amen.sh Version $AMEN_VERSION!"
BANNER_2="Print simplified vintage looking user interface elements in ASCII"
BANNER_3="Demonstration script"

init_amen
while true; do
	set_colors "$C_GREEN" "$C_BLACK"
	moveto 1 1
	p_hcenter "$BANNER_1" "$TMX" true; printf "\n"
	p_hcenter "$BANNER_2" "$TMX" true; printf "\n"
	p_hcenter "$BANNER_3" "$TMX" true; printf "\n"

	menu_hl=`expr $TMX \* 2 / 3`
	menu_x=`th_center "$TMX" "$menu_hl"`

	status_line "hostname -s" "whoami" "date"

	p_vmenu $menu_x 5 $menu_hl `expr $TMY - 7` \
		"Select a command to run" \
		"File Manager" \
		"Internet Browser" \
		"Bourne shell" \
		"Text editor" \
		"Exit"

	case "$?" in
		0)	deco || mc -b -a -d ;;
		1)
			site='mezzantrop.wordpress.com'
			lynx $site || links $site ;;
		2)	cls; sh ;;
		3)	vi || vim || emacs ;;
		4)	exit_amen ;;
	esac
	cls
done
