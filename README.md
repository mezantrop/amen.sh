# amen.sh
## amen.sh - (ASCII Menu) - Simplified vintage looking user interface elements for shell

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

[![amen.sh in action](https://mezzantrop.files.wordpress.com/2019/03/8bovsjhuzu.gif)](https://mezzantrop.files.wordpress.com/2019/03/8bovsjhuzu.gif)


### USAGE
```
#!/bin/sh
. ./amen.sh
init_amen
p_hcenter "Hello world!" "$TMX" true; printf "\n"
exit_amen
```

### HISTORY
* 2019.03.24	v0.1	zmey20000@yahoo.com	Initial release

### TODO
* Format text to be within windows
* Screen refresh, windows refresh
* Clean code, remove bash-isms
