#!/bin/sh

# "THE BEER-WARE LICENSE" (Revision 42):
# <zmey20000@yahoo.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.    Mikhail Zakharov

# amen.sh - (ASCII Menu)
# Simplified vintage looking user interface elements in ASCII

# -- USAGE --------------------------------------------------------------------
# #!/bin/sh
# . ./amen.sh
# init_amen
# p_hcenter "Hello world!" "$TMX" true; printf "\n"
# exit_amen

# -- HISTORY ------------------------------------------------------------------
# 2019.03.24	v0.1	zmey20000@yahoo.com	Initial release

# -- TODO ---------------------------------------------------------------------
# * Format text to be within windows
# * Screen refresh, windows refresh
# * Clean code, remove bash-isms



# -- Terminal capabilities ----------------------------------------------------
TMX=80				# Terminal maximun X
TMY=25				# Terminal maximum Y

# -- Interface borders and lines ----------------------------------------------
CTL='+'				# CTL, CTR, CBL, CBR - Corners
CTR='+'
CBL='+'
CBR='+'
LNH='-'				# Horisontal Line
LNV='|'				# Vertical line
SP=' '				# Space character

# -- COLORS -------------------------------------------------------------------
C_BLACK=0
C_RED=1
C_GREEN=2
C_YELLOW=3
C_BLUE=4
C_MAGENTA=5
C_CYAN=6
C_WHITE=7
C_DEFAULT=9

FG_DEFAULT=$C_DEFAULT		# Default terminal foreground color
BG_DEFAULT=$C_DEFAULT		# Default terminal background color

FG_COLOR=$C_GREEN		# Default amen foreground color
BG_COLOR=$C_BLACK		# Default amen background color

AMEN_VERSION="0.1"

# -- FUNCTIONS ----------------------------------------------------------------
exit_amen() {
	clear
	set_colors $FG_DEFAULT $BG_DEFAULT
	exit 0
}

trap exit_amen SIGINT SIGTERM

cls() {
	# Clear the screen

	printf "\033[2J"
}

set_colors() {
	# Set terminal colors
	# $1	- foreground
	# $2	- optional background

	printf "%b" "\033[01;3${1}m"
	[ -n $2 ] && printf "%b" "\033[01;4${2}m"
}

term_size() {
	# Gets terminal width and height. Sets global TMX TMY

	moveto 999 999
	getpos TMX TMY
}

init_amen () {
	# Gets terminal size and clears the screen

	term_size
	cls
	set_colors $FG_COLOR $BG_COLOR
}

getchar() {
	# Read a byte from STDIN and print it to STDOUT
	# $1 - Suppress terminal echo before and enable it after reading a byte

	[ "$1" ] && stty -icanon -echo
	dd bs=1 count=1 2>/dev/null
	[ "$1" ] && stty icanon echo
}

readkey() {
	# Read a character or an escape-sequence from STDIN, print it to STDOUT
	# $1 - Suppress terminal echo before and enable it after reading a byte
	# Return:
	#   0 - Escape sequence key
	#   1 - "Enter" key/no character
	#   2 - Any printable character
	#   3 - Unknown character/broken esc-sequence

	k=`getchar "$1"`
	case $k in
		$'\E')
			# ANSI Escape sequence
			[ "`getchar`" == '[' ] || return 3	# Broken esc-seq
			printf `getchar`
			return 0
			;;
		'')
			# Enter was pressed
			return 1
			;;
		[[:print:]])
			# Normal printable character
			printf "$k"
			return 2
			;;
		*)
			# Unknown character
			printf "$k"
			return 3
			;;
	esac
}

p_typewriter() {
	# Emulate typewriter printing

	# $1	- a file or a string to print
	# $2	- optional sleep timeout in seconds. Default is 0.001

	[ -f "$1" ] && p_cmd="cat" || p_cmd="printf"
	[ -n "$2" ] && s="$2" || s="0.001"

	"$p_cmd" "$1" | awk -v s="$s" -v p_cmd="$p_cmd" '
		BEGIN {FS=""}
		{
			for(i=1; i<=NF; i++) {
				printf("%s", $i);
				system("sleep "s)
			};

			if (p_cmd == "cat") print
		}'
}

getpos() {
	# Get current cursor position

	tset=`stty -g`
	stty -icanon -echo min 0 time 5
	printf '\033[6n'
	pos=`dd count=1 bs=9 2> /dev/null`
	stty "$tset"

	[ -n "$1" ] &&
		eval $1=`printf "%s" "$pos" | awk -F "[\[;R]" '{print $3}'`
	[ -n "$2" ] &&
		eval $2=`printf "%s" "$pos" | awk -F "[\[;R]" '{print $2}'`
}

moveto() {
	# Position cursor

	# $1, $2 	- X, Y

	printf "\033[$2;$1H"
}

p_line() {
	# Print a line of $5 chars either horisontal or vertical

	# $1, $2	- X, Y
	# $3		- Line length
	# $4		- Direction
	# $5		- Character

	ll=$3
	[ $ll -lt 1 ] && ll=1

	moveto $1 $2
	case $4 in
		v|V)
			for l in `seq 1 $ll`; do
				printf "%s" "$5"
				moveto $1 `expr $2 + $l`
			done
			;;
		h|H)
			for l in `seq 1 $ll`; do
				printf "%s" "$5"
			done
			;;
	esac
}

p_hline() {
	# Print a horisontal line "-"

	# $1, $2        - X, Y position
	# $3            - Line length

	p_line "$1" "$2" "$3" "H" "$LNH"
}

p_vline() {
	# Print a vertical line "|"

	# $1, $2        - X, Y position
	# $3            - Line length

	p_line "$1" "$2" "$3" "V" "$LNV"
}

p_space() {
	# Print a horisontal line of spaces

	# $1, $2        - X, Y position
	# $3            - Line length
	p_line "$1" "$2" "$3" "H" "$SP"
}

p_cblock() {
	# Uing space clear a block on the screen

	# $1, $2	- X, Y position
	# $3, $4	- Horisontal/Vertical length

	moveto $1 $2
	for v in `seq 1 $4`; do
		p_space $1 `expr $2 + $v - 1` $3
	done
}

th_center() {
	# Print position in $1 to print $2 in the center

	# $1	- 1-st length
	# $2	- 2-nd length

	[ "$1" -ge "$2" ] &&
		pos=`expr "$1" / 2 - "$2" / 2` ||
		pos=`expr "$2" / 2 - "$1" / 2`

		printf "%s" "$pos"
}

p_hcenter() {
	# Print the text in the center

	# $1	- Text to print
	# $2	- HL
	# $3	- typewriter emulation

	[ -z "$2" ] && hl="$TMX" || hl="$2"
	tl=`expr "$1" : '.*'`

	pos=`th_center "$hl" "$tl"`
	[ -n "$3" ] &&
		p_typewriter "\033[${pos}C${1}" ||
		printf "%b%s" "\033[${pos}C" "$1"
}

p_frame() {
	# Draw a frame

	#
	# +- Title -+  or no title  +-+
	# |         |               | |
	# +---------+               +-+
	#

	# $1, $2	- X, Y position
	# $3, $4	- Horisontal/Vertical length
	# $5		- Title or ""

	tx=$1; ty=$2; hl=$3; vl=$4

	[ $hl -lt 3 ] && hl=3
	[ $vl -lt 3 ] && vl=3

	moveto $tx $ty
	printf $CTL; p_hline `expr $tx + 1` $ty `expr $hl - 2`; printf $CTR
	p_vline $tx `expr $ty + 1` `expr $vl - 2`; printf $CBL
	p_vline `expr $tx + $hl - 1` `expr $ty + 1` `expr $vl - 2`
	p_hline `expr $tx + 1` `expr $ty + $vl - 1` `expr $hl - 2`; printf $CBR
	# Print a frame title
	[ -n "$5" ] && {
		tl=`expr "$5" : '.*' + 2`
		[ "$tl" -lt `expr "$3" - 2` ] && {
			txx=`th_center "$3" $tl`
			moveto `expr $txx + $tx` $ty;
			printf " %s " "$5";
		}
	}
}

p_window() {
	# Print a window with a title and clean background

	#
	# +- Title -+  or no title  +-+
	# |         |               | |
	# +---------+               +-+
	#

	# $1, $2	- X, Y position
	# $3, $4	- Horisontal/Vertical length
	# $5		- Title or ""

	p_frame "$1" "$2" "$3" "$4" "$5"
	p_cblock `expr $1 + 1` `expr $2 + 1` `expr $3 - 2` `expr $4 - 2`
}

idx2txt() {
	# Print menu item from the function "options" array by its index number
	# $1        - index
	# $2...     - Menu list

	idx=$1
	shift 1
	c=0
	for i in "$@"; do
		[ $c -eq $idx ] && printf "$i" && return $c
		c=`expr $c + 1`
	done
}

p_vmenu() {
 	# Print a vertical menu

	# +- Title -+
	# | Item 1  |
	# | Item 2  |
	# +-------- +

	# $1, $2        - X, Y
	# $3, $4        - HL, VL
	# $5		- Title or ""
	# $6 ...        - Menu items

	mtx=$1; mty=$2; hl=$3
	p_window $mtx $mty $hl $4 "$5"			# Draw window
	ihl=`expr $hl - 2`				# Inner horisontal len

	shift 5						# Skip to Menu items
	m_items=$#					# Number of menu items
	mix=`expr $mtx + 1`; miy=`expr $mty + 1`	# Min X,Y menu position
	may=`expr $miy + $m_items - 1`			# Max Y menu position
	micx=$mix; micy=$miy				# Current X,Y menu pos

	set_colors $BG_COLOR $FG_COLOR
	p_space $micx $micy $ihl
	set_colors $FG_COLOR $BG_COLOR

	# Print out menu then highlight the first item and move cursor there
	for item in "$@"; do
		moveto $micx $micy
		[ $micy -eq $miy ] && {
			set_colors $BG_COLOR $FG_COLOR
			p_hcenter "$item" $ihl
			set_colors $FG_COLOR $BG_COLOR
		} || p_hcenter "$item" $ihl
		micy=`expr $micy + 1`
	done
	micx=$mix; micy=$miy		# Reset current X,Y menu position and
	moveto $micx $micy		# set cursor to the upper item: Min X,Y

	# Read menu item selection
	key=0
	stty -icanon -echo		# No terminal echo allowed in menu mode
	while [ $key -ne 1 ]; do	# While key != Enter
		char=`readkey`		# Read character value
		key=$?			# Key: ESC|Enter|Dhar|Unknown

		[ $key -eq 0 ] && case $char in
			A|D)
				# Key Up/Left
				[ $micy -le $miy ] || {
					set_colors $FG_COLOR $BG_COLOR

					p_space $mix $micy $ihl
					idx=`expr $micy - $miy`
					item=`idx2txt $idx "$@"`
					moveto $micx $micy
					p_hcenter "$item" $ihl

					micy=`expr $micy - 1`
					set_colors $BG_COLOR $FG_COLOR

					p_space $mix $micy $ihl
					idx=`expr $micy - $miy`
					item=`idx2txt $idx "$@"`
					moveto $micx $micy
					p_hcenter "$item" $ihl

					set_colors $FG_COLOR $BG_COLOR
				}
				;;
			B|C)
				# Key Down/Right
				[ $micy -ge $may ] || {
					set_colors $FG_COLOR $BG_COLOR

					p_space $mix $micy $ihl
					idx=`expr $micy - $miy`
					item=`idx2txt $idx "$@"`
					moveto $micx $micy
					p_hcenter "$item" $ihl

					micy=`expr $micy + 1`
					set_colors $BG_COLOR $FG_COLOR

					p_space $mix $micy $ihl
					idx=`expr $micy - $miy`
					item=`idx2txt $idx "$@"`
					moveto $micx $micy
					p_hcenter "$item" $ihl

					set_colors $FG_COLOR $BG_COLOR
				}
				;;
			*)
				# Unknown keys die here
				;;
		esac
	done

	stty icanon echo		# Turn echo back again
	return `expr $micy - $miy`	# Return the item index counting from 0
}

p_inverted() {
	# Print text with inverted color
	# $1	- Text to print
	# $2	- Use typewriter when printing

	set_colors $BG_COLOR $FG_COLOR
	[ -n $2 ] && p_typewriter "$1" || printf "%s" "$1"
	set_colors $FG_COLOR $BG_COLOR
}

p_caption() {
	# Print informative box with 2 buttons

	#
	# +------- Title ------+
	# |                    |
	# | Some caption line  |
	# |                    |
	# | " Btn0 "  " Btn1 " |
	# +--------------------+
	#

	# $1, $2	- X, Y
	# $3		- HL
	# $4		- Title or ""
	# $5		- Caption line
	# $6		- Button0
	# $7		- Button1

	p_window $1 $2 $3 7 "$4"			# Print window
	moveto `expr $1 + 2` `expr $2 + 2`
	printf "%.*s" `expr $3 - 4` "$5"		# Print caption
	moveto `expr $1 + 2` `expr $2 + 4`
	p_inverted "$6"; printf "\t%s" "$7"		# Print buttons

	# Draw buttons
	moveto `expr $1 + 2` `expr $2 + 4`		# Goto the first element
	btn=0
	key=0
	stty -icanon -echo		# No terminal echo allowed in menu mode
	while [ $key -ne 1 ]; do	# While key != Enter
		char=`readkey`		# Read character value
		key=$?			# Key: ESC|Enter|Dhar|Unknown

		[ $key -eq 0 ] &&
			case $char in
				A|D)
					# Key Up/Left
					moveto `expr $1 + 2` `expr $2 + 4`
					p_inverted "$6"; printf "\t%s" "$7"
					btn=0
					;;
				B|C)
					# Key Down/Right
					moveto `expr $1 + 2` `expr $2 + 4`
					printf "%s\t" "$6"; p_inverted "$7"
					btn=1
					;;
				*)
					# Unknown keys die here
					;;
			esac
	done

	stty icanon echo		# Turn echo back again
	return $btn			# Return selected button index from 0
}

p_inbox() {
	# Print input box with 2 buttons

	#
	# +------- Title ------+
	# |                    |
	# | Input line 1234567 |
	# |                    |
	# | " Btn0 "  " Btn1 " |
	# +--------------------+
	#

	# $1, $2	- X, Y
	# $3		- HL
	# $4		- Title or ""
	# $5		- Button0
	# $6		- Button1

	p_window "$1" "$2" "$3" 7 "$4"			# a window and the title
	moveto `expr $1 + 2` `expr $2 + 2`

	# Print an input line in inverted color

	set_colors $BG_COLOR $FG_COLOR
	p_space `expr $1 + 2` `expr $2 + 2` `expr $3 - 4`
	moveto `expr $1 + 2` `expr $2 + 2`; read instr
	set_colors $FG_COLOR $BG_COLOR

	moveto `expr $1 + 2` `expr $2 + 4`
	p_inverted "$5"; printf "\t%s" "$6"		# Print buttons

	# Draw buttons
	moveto `expr $1 + 2` `expr $2 + 4`		# Goto the first element
	btn=0
	key=0
	stty -icanon -echo		# No terminal echo allowed in menu mode
	while [ $key -ne 1 ]; do	# While key != Enter
		char=`readkey`		# Read character value
		key=$?			# Key: ESC|Enter|Dhar|Unknown

		[ $key -eq 0 ] &&
			case $char in
				A|D)
					# Key Up/Left
					moveto `expr $1 + 2` `expr $2 + 4`
					p_inverted "$5"; printf "\t%s" "$6"
					btn=0
					;;
				B|C)
					# Key Down/Right
					moveto `expr $1 + 2` `expr $2 + 4`
					printf "%s\t" "$5"; p_inverted "$6"
					btn=1
					;;
				*)
					# Unknown keys die here
					;;
			esac
	done

	stty icanon echo		# Turn echo back again

	[ $btn -eq 0 ] && printf "%s" $instr || printf ""
	return $btn
}

status_line() {
	# Print a status line (inverted textline) at the bottom of the screen
	# Parameters are shell comands to run

	set_colors $BG_COLOR $FG_COLOR
	p_space 1 "$TMY" "$TMX"
	set_colors $FG_COLOR $BG_COLOR

	moveto 1 "$TMY"
	sln=""
	for cmd in "$@"; do
		sln="$sln"`$cmd`" | "
	done

	p_inverted "`printf "%.${TMX}s" "$sln"`"
}

# -- TESTS --------------------------------------------------------------------
run_demo() {
	init_amen

	# static elements
	p_frame 1 1 20 20 "Hello world!"
	status_line "whoami" "uptime" "hostname"

	# Interactive elements
	p_inbox 22 1 20 "Input box" "Yes" "No"
	printf "Button: %d" $?

	p_caption 42 1 20 "Title ln" "Some text" "Yes" "No"
	printf "Button: %d" $?

	read
	exit_amen
}

# Uncomment the line below to run a simple demonstration
# run_demo
